@extends('layouts.app')

@section('content')

    <div class="container chats">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @if(isset($receiver))
                <chat-component 
                    :user="{{ auth()->user() }}"
                    :receiver="{{$receiver}}"
                ></chat-component>
                @endif
            </div>
            <div class="col-md-4">
                <ul class="list-group">
                    <li class="list-group-item" v-for="user in users">
                        <chat-user url="{{ route('chat.user') }}/" :user="user"></chat-user>
                        <span v-if="user.typing" class="badge badge-primary">typing...</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endsection
