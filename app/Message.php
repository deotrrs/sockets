<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['message','send_to'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function send_to()
    {
        return $this->belongsTo('App\User','send_to');
    }
}
