<?php

namespace App\Http\Controllers;

use App\Events\MessageSent;
use App\Events\PrivateMessage;
use App\Message;
use Illuminate\Http\Request;
use App\User;

class ChatsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('chat');
    }

    public function individualMessage(Request $request)
    {
        $data['receiver'] = User::where('slug',$request->slug)->first();
        return view('chat', $data);
    }

    public function fetchMessages(Request $request)
    {
        $user = auth()->user();
        $message = Message::with(['user','send_to'])
        ->where(function($query) use ($user, $request){
            $query->where('user_id', $user->id)
            ->where('send_to',  $request->receiver);
        })
        ->orWhere(function($query) use ($user, $request){
            $query->where('user_id', $request->receiver)
            ->where('send_to',  $user->id);
        });
        return $message->get();
    }

    public function sendMessage(Request $request)
    {   
        $receiver =  $request->receiver;
        $message = auth()->user()->messages()->create([
            'send_to' => $receiver['id'],
            'message' => $request->message
        ]);

        broadcast(new PrivateMessage(auth()->user(), $message, $receiver))->toOthers();// private channel broadcast
		broadcast(new MessageSent(auth()->user(), $message))->toOthers();// presence channel broadcast
        
        return ['status' => 'Message Sent!'];
    }
}
